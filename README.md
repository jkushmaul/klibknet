klibknet [![pipeline status](https://gitlab.com/jkushmaul/klibknet/badges/master/pipeline.svg)](https://gitlab.com/jkushmaul/klibknet/commits/master) [![coverage report](https://gitlab.com/jkushmaul/klibknet/badges/master/coverage.svg)](https://gitlab.com/jkushmaul/klibknet/commits/master)

I wanted to write an abstracted "event" driven network library
Depends on libktypes for klist_t

* Source: https://gitlab.com/jkushmaul/klibknet
* Docs: https://jkushmaul.gitlab.io/klibknet/index.html
* Coverage: https://jkushmaul.gitlab.io/klibknet/coverage/index.html
* Releases: https://gitlab.com/api/v4/projects/3618443/releases
** Ask gitlab to implement a UI for it.

TODO:
  * make sure it's ipv6 capable
  * async dns
  * determine how to make it multithreaded
    I wasn't concerned with multithreading to begin with.  Perhaps easiest is to use threads internally on loops
  * Unify udp and tcp better (use connect for udp too)
  * epoll with udp (once above is done)
  * create callbacks, or callback layer to associate to addrs.
      May cause a lot of extra memory - Every client would get min of 8 bytes (possibly one for read, write, exception)
      * interrupt - gets callback when interrupt triggered
      * listener - gets callback when ready which it can call accept with.
      * writes - writes can get a callback that then closes connection
      * reads - when data is available, callback that can read the data - this is about the only case i have right now, handle_client.
      * This prevents a second loop outside of the main service loop.
  * Move to allocate/free within functions, make tests allocate and call cleanup (like hashmap).  The only use I've had for not calling free
    is in tests not having to call alloc and free.  But it makes for edge cases of memory leaks.
Easily write a service (in C)
Example is in integrationtest



