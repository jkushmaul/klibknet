#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stddef.h>


#include <knet2_epoll.h>
#include <kservice.h>

int handle_io_read(kservice_t *service, kservice_client_t *client) {
	knet2_error_t error = {0};
	const int buflen = 4096;
	uint8_t buffer[buflen];
	
	int n = service->network_handler->recv(service->network_handler, (knet2_ip_addr_t*)client->addr, buffer, buflen, &error);
	char remote[KSERVICE_MAX_ADDR_LENGTH]={0};
	service->network_handler->addr_reverse(client->addr, remote, sizeof(remote), &error);
	if (n <= 0) {
		//remove it from list.  <0 error, 0 closed.
		if (n < 0) {
			printf("error reading from %s\n", remote);
			return -1;
		}
	} else {
		printf("Read %d bytes from %s\n", n, remote);
		kservice_queue_write_allocs(service, client, buffer, n);
	}
	return 1;
}

int handle_io_write(kservice_t *service, kservice_client_t *client) {
	knet2_error_t error = {0};
	int n = kservice_write_client(service, client, &error);
	if (n == 0) {
		printf("Client finished, destroying\n");
		return 0;
	} else if (n < 0 ) {
		return -1;
	} else {
		printf("Client not finished, not destroying\n");
		return 1;
	}
}

void handle_io_event(kservice_t *service, kservice_client_t *client, knet2_event_t *event) {
	int result = 0;
	//0 = done - destroy; -1 = error - destroy, 1 = work left to be done.
    if (event->flags & KNET2_EV_IN) {
		result = handle_io_read(service, client);
    }
    if(result >= 0 && event->flags & KNET2_EV_OUT) {
		result = handle_io_write(service, client);
    }
    if (result <=0 || event->flags & KNET2_EV_ERR) {
		kservice_destroy_client_frees(service, client);
    }
}

void handle_loop_after(kservice_t *service, void *context) 
{
    //context is null, I don't care.
    printf("handle_loop_after\n");
    service->should_run = 0;
}

static kservice_t service;
static knet2_epoll_handler_t epoll_h;
static knet2_ip_handler_t *handler = (knet2_ip_handler_t *) &epoll_h;

void sig_handler (int sig) {
    knet2_error_t error;
    switch(sig) {
        case SIGINT:
            printf("ERROR: Received SIGINT, telling service to shut down\n");
            //just to demonstrate interrupting, could just as easily set shutdown to 0
			kservice_add_loop_complete_callback(&service, NULL, handle_loop_after);
			knet2_interrupt(service.network_handler, &error);
            break;
        case SIGTSTP:
            printf("ERROR: Received SIGTSTP, entering CLI\n");
			kservice_add_loop_complete_callback(&service, NULL, handle_loop_after);
            knet2_interrupt(service.network_handler, &error);
            break;
    }
}


//TODO: Setup timer to send interrupt, or, use loop complete callback and small jittery checks.
int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("usage: %s [binding ip:port]\n", argv[0]);
        return -1;
    }
    const char *binding = argv[1];
    
    knet2_error_t knet2_error;
    knet2_error_t errort = { 0 };
    if (knet2_epoll_handler_init_allocs(&epoll_h, 1000, &errort)) {
        printf("Error initializing network\n");
        return -2;
    }
    epoll_h.ip_base.select_timeout_usec = -1;
    handler->addr_parse(&handler->this_addr, binding, &knet2_error);
    
    memset(&service, 0, sizeof(kservice_t));
    kservice_init(&service, (knet2_ip_handler_t*) handler, sizeof(kservice_client_t), 5);
    service.handle_io_event = handle_io_event;
    
    if (signal(SIGINT, sig_handler) == SIG_ERR) {
        printf("\ncan't catch SIGINT\n");
        return -1;
    }
        
    int error;
    printf("Attempting to start service on %s\n", binding);
    error = kservice_start(&service, &knet2_error);
    if (error){
        printf("Could not start service\n");
    } else {
        printf("Service loop\n");
        kservice_loop(&service, &knet2_error);
    }
    
    printf("Shutting down\n");
    kservice_shutdown_frees(&service);
    
    return error;
}
