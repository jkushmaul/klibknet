#include "tests.h"

extern "C" {
#include "knet2_select.h"
}

static knet2_error_t knet2error;


TEST(knet2_select, knet2_select_handler_init){
	knet2_select_handler_t tcp = { 0 };
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_select_init(&tcp, &error), 0);
	knet2_select_handler_t *handler = &tcp;
	ASSERT_EQ(knet2_ip_addr_parse, handler->ip_base.addr_parse);
	ASSERT_EQ(sizeof(knet2_ip_addr_t), handler->ip_base.sizeof_addr);
	ASSERT_EQ((char*)knet2_select_wait, (char*)(handler->ip_base.wait));
	knet2_select_release_frees((knet2_ip_handler_t*)&tcp);
}
TEST(knet2_ip, listen_connct_accept_select_zero_after_connect){
	const char *addr = "127.0.0.1:60004";
	knet2_select_handler_t net = { 0 };
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_select_init(&net, &error), 0);
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&net.ip_base.this_addr), addr, &knet2error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &knet2error);
	ASSERT_EQ(0, result);

	knet2_ip_addr_t srv;
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&srv), addr, &knet2error);
	result = knet2_ip_connect((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&srv, &knet2error);
	ASSERT_EQ(0, result);


	knet2_ip_addr_t cli;
	result =  0;
	memset(&cli, 0, sizeof(knet2_ip_addr_t));
	int count = 0;
	while (result != 1 && count++ < 5) {
		sleep(1);
		result = knet2_ip_accept((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli, &knet2error);
	}
	ASSERT_EQ(1, result);

	const size_t MAX_EVENTS = 1024;
	knet2_event_t events[MAX_EVENTS];
	result = knet2_select_wait((knet2_ip_handler_t*)&net, events, MAX_EVENTS, &knet2error);
	ASSERT_EQ(0, result);

	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&net.ip_base.this_addr));
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&srv));
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli);

	knet2_select_release_frees((knet2_ip_handler_t*)&net);
}

TEST(knet2_select, listen_connct_accept_select_send_recv){
	const char *addr = "127.0.0.1:60005";
	knet2_select_handler_t net = { 0 };
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_select_init(&net, &error), 0);
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&net.ip_base.this_addr), addr, &knet2error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &knet2error);
	ASSERT_EQ(0, result);

	knet2_ip_addr_t srv;
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&srv), addr, &knet2error);
	result = knet2_ip_connect((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&srv, &knet2error);
	ASSERT_EQ(0, result);


	knet2_ip_addr_t cli;
	memset(&cli, 0, sizeof(knet2_ip_addr_t));
	result = knet2_ip_accept((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli, &knet2error);
	ASSERT_EQ(1, result);

	const size_t MAX_EVENTS = 1024;
	knet2_event_t events[MAX_EVENTS];
	result = knet2_select_wait((knet2_ip_handler_t*)&net, events, MAX_EVENTS, &knet2error);
	ASSERT_EQ(0, result);

	char buffer[128] = "Hello there!";
	int n = knet2_ip_send((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&srv, (uint8_t*)buffer, strlen(buffer) + 1, &knet2error);
	//0 doesn't seem so awesome at this point.  intention was pass or fail, not between.
	ASSERT_EQ(strlen(buffer) + 1, n);

	result = knet2_select_wait((knet2_ip_handler_t*)&net, events, MAX_EVENTS, &knet2error);
	//result will be <0 on error, 0 on success.
	ASSERT_EQ(1, result);
	ASSERT_EQ(cli.sockfd, events[0].fd);//this is showing up as net.this_addr.sockfd instead... :?
	ASSERT_EQ(0, events[0].flags);


	memset(buffer, 0, sizeof(buffer));
	n = knet2_ip_recv((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli, (uint8_t*)buffer, sizeof(buffer), &knet2error);
	ASSERT_EQ(strlen("Hello there!") + 1, n);
	ASSERT_TRUE(!strcmp("Hello there!", buffer));

	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&net.ip_base.this_addr));
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&srv));
	knet2_select_release_frees((knet2_ip_handler_t*)&net);

}
