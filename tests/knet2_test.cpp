#include "tests.h"

extern "C" {
    #include "knet2.h"
    #include <string.h>
}

static knet2_error_t knet2error;

TEST(knet2_ip, knet2_ip_addr_resolve_host) {
	const char *strhostip = "localhost";
	knet2_ip_host_port_t hostip;
	knet2_error_t error;

	memset(&error, 0, sizeof(knet2_error_t));
	int result = knet2_ip_addr_resolve_host(strhostip, (knet2_ip_addr_t*)&hostip, &error);
	char *errormsg = error.knet2_errmsg;
	printf("errormsg=%s\n", errormsg);
	ASSERT_EQ(0, result);
}

TEST(knet2_ip, knet2_ip_parse_host_port_success){
	const char *strhostip = "127.0.0.1:80";
	knet2_ip_host_port_t hostip;
	int result = knet2_ip_parse_host_port(&hostip, strhostip);

	ASSERT_EQ(0, result);
	ASSERT_EQ(strlen("127.0.0.1"), strlen(hostip.host));
	ASSERT_TRUE(!strcmp("127.0.0.1", hostip.host));
	ASSERT_EQ(strlen("80"), strlen(hostip.port));
	ASSERT_TRUE(!strcmp("80", hostip.port));
}

TEST(knet2_ip, knet2_ip_parse_host_port_mising_colon){
	const char *strhostip = "127.0.0.1";
	knet2_ip_host_port_t hostip = { 0 };
	int result = knet2_ip_parse_host_port(&hostip, strhostip);

	ASSERT_EQ(-1, result);
}

TEST(knet2_ip, knet2_ip_parse_host_port_mising_port){
	const char *strhostip = "127.0.0.1:";
	knet2_ip_host_port_t hostip = { 0 };
	int result = knet2_ip_parse_host_port(&hostip, strhostip);

	ASSERT_EQ(-2, result);
}

TEST(knet2_ip, knet2_ip_parse_host_port_bad_port_chars){
	const char *strhostip = "127.0.0.1:abc";
	knet2_ip_host_port_t hostip = { 0 };
	int result = knet2_ip_parse_host_port(&hostip, strhostip);

	ASSERT_EQ(-3, result);
}

TEST(knet2_ip, knet2_ip_parse_host_port_bad_port_size){
	const char *strhostip = "127.0.0.1:65536";
	knet2_ip_host_port_t hostip = { 0 };
	int result = knet2_ip_parse_host_port(&hostip, strhostip);

	ASSERT_EQ(-6, result);
}

TEST(knet2_ip, knet2_ip_addr_parse){
	knet2_ip_addr_t vaddr;
	const char* str_addr = "127.0.0.1:80";
	int result = knet2_ip_addr_parse((knet2_ip_addr_t*)&vaddr, str_addr, &knet2error);

	ASSERT_EQ(0, result);
	//TODO: better test here, I removed str check.  Should be more concerned with proper saddr setup.
}

TEST(knet2_ip, knet2_ip_address_reverse){
	const char *actual = "127.0.0.1:12345";
	knet2_ip_addr_t addr;

	memset(&addr, 0, sizeof(knet2_ip_addr_t));
	int result = knet2_ip_addr_parse((knet2_ip_addr_t*)&addr, actual, &knet2error);
	ASSERT_EQ(0, result);

	char str_addr[256];
	memset(str_addr, 0, sizeof(str_addr));
	result = knet2_ip_address_reverse((knet2_ip_addr_t*)&addr, str_addr, 256, &knet2error);
	ASSERT_EQ(0, result);
	ASSERT_EQ(strlen(actual), strlen(str_addr));
	ASSERT_TRUE(!strcmp(actual, str_addr));
}


TEST(knet2_ip, knet2_ip_handler_init){
	knet2_ip_handler_t ip;
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_ip_init(&ip, &error), 0);

	knet2_ip_handler_t *handler = &ip;
	ASSERT_EQ(knet2_ip_addr_parse, handler->addr_parse);
	ASSERT_EQ(sizeof(knet2_ip_addr_t), handler->sizeof_addr);
	ASSERT_EQ(knet2_ip_release_addr, handler->release_addr);
	ASSERT_EQ(knet2_ip_addr_parse, handler->addr_parse);
	ASSERT_EQ((char*)knet2_ip_accept, (char*)(handler->accept));
	ASSERT_EQ((char*)knet2_ip_send, (char*)(handler->send));
	ASSERT_EQ((char*)knet2_ip_recv, (char*)(handler->recv));
	ASSERT_GT(ip.interrupt_fd, 0);

}


TEST(knet2_tcp, listen_fails){
	const char *hostip = "8.8.8.8:60000";
	knet2_ip_handler_t net;
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_ip_init(&net, &error), 0);
	knet2_ip_addr_parse(&net.this_addr, hostip, &knet2error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &knet2error);
	ASSERT_EQ(-4, result);
}

TEST(knet2_ip, listen_passes){
	const char *hostip = "127.0.0.1:60000";
	knet2_ip_handler_t net;
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_ip_init(&net, &error), 0);
	knet2_ip_addr_parse(&net.this_addr, hostip, &knet2error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &knet2error);
	ASSERT_EQ(0, result);
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, &net.this_addr);
}

TEST(knet2_ip, accept_not_block){
	const char *hostip = "127.0.0.1:60001";
	knet2_ip_handler_t net;

	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_ip_init(&net, &error), 0);

	knet2_ip_addr_parse(&net.this_addr, hostip, &knet2error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &knet2error);
	ASSERT_EQ(0, result);
	knet2_ip_addr_t cli;
	memset(&cli, 0, sizeof(knet2_ip_addr_t));
	result = knet2_ip_accept((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli, &knet2error);
	ASSERT_EQ(0, result);
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&net.this_addr));
}

TEST(knet2_ip, listen_connect_success){
	const char *addr = "127.0.0.1:60002";
	knet2_ip_handler_t net;
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_ip_init(&net, &error), 0);
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&net.this_addr), addr, &knet2error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &knet2error);
	ASSERT_EQ(0, result);

	knet2_ip_addr_t srv;
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&srv), addr, &knet2error);
	result = knet2_ip_connect((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&srv, &knet2error);
	ASSERT_EQ(0, result);
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&net.this_addr));

	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&srv));
}
TEST(knet2_ip, listen_connct_accept_success){
	const char *addr = "127.0.0.1:60003";
	knet2_ip_handler_t net;
	knet2_error_t error = { 0 };

	ASSERT_EQ(knet2_ip_init(&net, &error), 0);
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&net.this_addr), addr, &knet2error);
	int result = knet2_ip_listen((knet2_ip_handler_t*)&net, &knet2error);
	ASSERT_EQ(0, result);

	knet2_ip_addr_t srv;
	knet2_ip_addr_parse((knet2_ip_addr_t*)(&srv), addr, &knet2error);
	result = knet2_ip_connect((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&srv, &knet2error);
	ASSERT_EQ(0, result);

	knet2_ip_addr_t cli;
	memset(&cli, 0, sizeof(knet2_ip_addr_t));
	result = 0;
	int i = 0;
	while (result != 1 && i++ < 3) {
		result = knet2_ip_accept((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli, &knet2error);
		sleep(1);
	}
	ASSERT_EQ(1, result);

	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&net.this_addr));
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)(&srv));
	knet2_ip_release_addr((knet2_ip_handler_t*)&net, (knet2_ip_addr_t*)&cli);
}
