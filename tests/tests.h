#ifndef TESTS_H
#define TESTS_H

#include <gtest/gtest.h>
#include <atomic>
using namespace std;
extern "C" {
#include <klist.h>
#include <knet2.h>
}
 
//God damn testing libraries don't use same defines, and worse, they swap their expected, actual (UnitTest++ -> gtest) "I do what I want"
#define REQUIRE
#define CHECK_EQUAL(expected,actual) ASSERT_EQ(actual, expected)
#define CHECK(expression) ASSERT_TRUE(expression)


void inline create_tcp_addr(knet2_ip_addr_t *addr, const char *host_ip)
 { 
     knet2_error_t tmp;
     int result = knet2_ip_addr_parse((knet2_ip_addr_t*)addr, host_ip, &tmp);
     ASSERT_EQ(0, result) << "knet2_ip_addr_parse==0";
     return;
}

typedef struct mock_send_data_t {
    uint8_t buffer[1024];
    size_t len;
    int do_override_return_value;
    size_t override_return_value;
} mock_send_data_t;

static mock_send_data_t mock_send_data;

size_t inline mock_send(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli, uint8_t *buffer, size_t buffer_size)
{
    if (mock_send_data.do_override_return_value) {
        return mock_send_data.override_return_value;
    } else {
        memcpy(mock_send_data.buffer, buffer, buffer_size);
        return buffer_size;
    }
}

int  inline mock_connect(knet2_ip_handler_t *vnet, knet2_ip_addr_t *vcli) 
{
    return 0;
}

int  inline mock_addr_parse(knet2_ip_addr_t *vaddr, const char *host)
{
    return 0;
}
void  inline mock_release_addr(knet2_ip_addr_t *vaddr)
{ 
}




void inline test_free_list(klist_t *nnd, int freevalue)
{
        klist_node_t *n = 0;
        n= nnd->head;
        while (n) {
            klist_node_t *thisnode = n;
            n = n->next;
            klist_remove_node(nnd, thisnode);
            if (freevalue) {
                free(thisnode->value);
            }
            free(thisnode);
        }
}

#endif
